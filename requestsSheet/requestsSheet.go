/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
package requestssheet

import (
	"context"
	"log"

    "worker/config"

	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
)

type RequestsSheet struct {
	SheetsService *sheets.Service
	sheetId       string
    config        *config.Config
}

func NewRequestsSheet(conf *config.Config) *RequestsSheet {
	const jsonPath = "./credentials.json"
	pc := &RequestsSheet{}
    pc.config = conf
	pc.sheetId = conf.Requests.SheetId
	if err := pc.createService(jsonPath); err != nil {
		log.Fatal(err)
	}
	return pc
}

func (r *RequestsSheet) createService(credentialsPath string) error {
	var err error
	ctx := context.Background()
	r.SheetsService, err = sheets.NewService(ctx, option.WithCredentialsFile(credentialsPath))
	return err
}

func (r *RequestsSheet) Add(data ...interface{}) error {
	searchRange := "'Заявки'!B:B"
	call := r.SheetsService.Spreadsheets.Values.Append(r.sheetId, searchRange, &sheets.ValueRange{
		Values: [][]interface{}{
            data,
		},
	})
	call.ValueInputOption("USER_ENTERED")
	_, err := call.Do()
	if err != nil {
		log.Printf("[worker:RequestsSheet] schedule request back and initializing 20 seconds wait due to error:\n\t%v\n", err)
		return err
	} else {
		log.Printf("Request added to RequestsSheet: %v\n", data)
		return nil
	}
}
