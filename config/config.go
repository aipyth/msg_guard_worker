/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
package config

import (
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Requests struct {
		SheetId    string `yaml:"sheet-id"`
        WriteRange string `yaml:"write-range"`
	}

	Base struct {
		SheetId         string `yaml:"sheet-id"`
        ReadRange       []string `yaml:"read-range"`
        SheetDataLimit  int     `yaml:"sheet-data-limit"`

        FlatRegexp      string `yaml:"address-regexp"`
        FlatLowercase   bool   `yaml:"address-lowercase"`
        FlatsRowStart   int    `yaml:"address-row-start"`
        FlatNameCol     int    `yaml:"address-name-col"`

		PhoneNumberCols []int  `yaml:"phone-number-cols"`
		VINCols         []int  `yaml:"vin-cols"`
        BadassColumns   []int  `yaml:"badasses"`
        DisableColumns  []int  `yaml:"disable"`
	}

	Payments struct {
		SheetId             string  `yaml:"sheet-id"`
        ReadRange           string  `yaml:"read-range"`
        SheetDataLimit      int     `yaml:"sheet-data-limit"`

        FlatRegexp          string  `yaml:"address-regexp"`
        FlatLowercase       bool    `yaml:"address-lowercase"`
        FlatsRowStart       int     `yaml:"address-row-start"`
        FlatNameCol         int     `yaml:"address-name-col"`

        YearStartCol        int     `yaml:"year-start-col"`
        YearRow             int     `yaml:"year-row"`
        YearMonthIndexDelta int     `yaml:"year-month-index-delta"`

        YearStart           int    `yaml:"year-start"`
        MonthStart          int    `yaml:"month-start"`

        PaymentThreshold    int    `yaml:"payment-threshold"`
	}

	Timers struct {
		LastMonthsPayments int `yaml:"last-months-payments"`
		Unpaid             int `yaml:"unpaid"`
		VIN                int `yaml:"vin"`
		PhoneNumbers       int `yaml:"phone"`
		NewRequests        int `yaml:"new-requests"`
		DenyRequests       int `yaml:"deny-requests"`
        Flats              int `yaml:"flats"`
        Badass             int `yaml:"badass"`
        Disable            int `yaml:"disable"`
	}
}

func GetConfig() *Config {
	contents, err := ioutil.ReadFile("./config.yml")
	if err != nil {
		log.Panic(err)
	}

	conf := new(Config)
	err = yaml.Unmarshal(contents, conf)
	if err != nil {
		log.Panic(err)
	}
	return conf
}
