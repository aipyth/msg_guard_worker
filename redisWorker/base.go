/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
package redisWorker

import (
	"context"
	"log"
	"strings"
)

const flatVINKeyPrefix = "vin:"
const flatPhoneNumbersKeyPrefix = "flatPhoneNumbers:"
const flatsKey = "flats"
const badassAddressesKey = "badasses"
const deactivatedKey = "deactivated"

func (w Worker) transformBaseAddress(address string) string {
    if w.Config.Base.FlatLowercase {
        address = strings.ToLower(address)
    }
    return strings.TrimSpace(address)
}

func (w *Worker) updateBadass() {
   addresses := w.baseSheet.GetBadassAddresses()

   w.redisClient.Del(context.Background(), badassAddressesKey)
   for _, address := range addresses {
       address = w.transformBaseAddress(address)
       _, err := w.redisClient.SAdd(context.Background(), badassAddressesKey, address).Result()
       if err != nil {
           log.Printf("[Worker:payments] redis error %v\n", err.Error())
       }
   }
}

func (w *Worker) updateVIN() {
	vins := w.baseSheet.GetVINs()

	for flat, vin := range vins {
        flat = w.transformBaseAddress(flat)
		// delete the key as all the flat vins are available through the executed request
		// old values must be deleted
		w.redisClient.Del(context.Background(), flatVINKeyPrefix+flat)
		for _, vn := range vin {
			_, err := w.redisClient.SAdd(context.Background(), flatVINKeyPrefix+flat, vn).Result()
			if err != nil {
				log.Println("[Worker] error writing VIN", err)
			}
		}
	}
}

func (w *Worker) updatePhoneNumbers() {
	phoneNumbers := w.baseSheet.GetPhoneNumbers()

	for flat, phns := range phoneNumbers {
        flat = w.transformBaseAddress(flat)
		// delete the key as all the flat numbers are available through the executed request
		// old values must be deleted
		w.redisClient.Del(context.Background(), flatPhoneNumbersKeyPrefix+flat)
		for _, phn := range phns {
			_, err := w.redisClient.SAdd(context.Background(), flatPhoneNumbersKeyPrefix+flat, phn).Result()
			if err != nil {
				log.Println("[Worker] error writing phone number", err)
			}
		}
	}
}

func (w *Worker) updateFlats() {
    flats := w.baseSheet.GetFlats()

    // delete the key as all the flat numbers are available through the executed request
    // old values must be deleted
    w.redisClient.Del(context.Background(), flatsKey)
    for _, flat := range flats {
        flat = w.transformBaseAddress(flat)
        _, err := w.redisClient.SAdd(context.Background(), flatsKey, flat).Result()
        if err != nil {
            log.Println("[Worker] error adding flat to flats set", err)
        }
    }
}

func (w *Worker) updateDeactivated() {
    addresses := w.baseSheet.GetDeactivated()

    deactivated := make(map[string]struct{})
    recentDeactivated := w.redisClient.SMembersMap(context.Background(), deactivatedKey).Val()
    for _, addr := range addresses {
        if _, ok := recentDeactivated[addr]; !ok {
            w.deactivateUserAddress(addr)
            deactivated[addr] = struct{}{}
        }
    }
    for addr := range recentDeactivated {
        if _, ok := deactivated[addr]; !ok {
            w.reactivateUserAddress(addr)
        }
    }
}

func (w *Worker) deactivateUserAddress(address string) {
	const query = "UPDATE users SET activated = FALSE WHERE address = $1;"
	_, err := w.postgresClient.Exec(query, address)
	if err != nil {
		log.Printf("Error while deactivating for %v. %v\n", address, err)
		return
	}
    log.Printf("[Worker] deactivated %s\n", address)
}

func (w *Worker) reactivateUserAddress(address string) {
	const query = "UPDATE users SET activated = TRUE WHERE address = $1 AND activated = FALSE;"
	_, err := w.postgresClient.Exec(query, address)
	if err != nil {
		log.Printf("Error while deactivating for %v. %v\n", address, err)
		return
	}
    log.Printf("[Worker] reactivated %s\n", address)
}
