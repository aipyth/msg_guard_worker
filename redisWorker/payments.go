/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
package redisWorker

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	redis "github.com/go-redis/redis/v8"
)

const currentMonthPaymentsKey = "last-months-payments"
const lastMonthOfDisablingKey = "disabled-month"
const unpaidAddressKey = "unpaid:"

// turn2Dto1D flattens some array of size (n, m) to array of size (n * m)
func turn2Dto1D(arr [][]string) []string {
	if len(arr) == 0 {
		return []string{}
	}
	res := make([]string, len(arr)*len(arr[0]))

	for i, v := range arr {
		for j, k := range v {
			res[i*len(v)+j] = k
		}
	}
	return res
}

func (w *Worker) sumPayments(paymentsSheets ...[][]string) [][]string {
	if len(paymentsSheets) == 0 {
		log.Println("[WARNING] 0 payments sheets were passed to sumPayments function. Got no results.")
		return [][]string{}
	}
	results := make([][]string, len(paymentsSheets[0]))
    
	var prevSum float64
	var toAdd float64
	var newSum string
	var err error
	for _, sheet := range paymentsSheets {
		for i, row := range sheet {
            // some citizens are msg guard employees. and they do not pay for the service.
            // so mark their payments as minimal
            if row[1] == "*" {
                row[1] = strconv.Itoa(w.Config.Payments.PaymentThreshold)
            }

			if len(results[i]) == 0 {
				results[i] = append(results[i], row[0], row[1])
			} else {
				prevSum, err = strconv.ParseFloat(results[i][1], 64)
				if err != nil {
					// log.Println("[summing payments]:sumPayments: error converting prevSum to int", results[i][1])
				}
				toAdd, _ = strconv.ParseFloat(row[1], 64)
				newSum = strconv.FormatFloat(prevSum + toAdd, 'f', 2, 64)
				results[i][1] = newSum
			}
		}
	}
	return results
}

func (w Worker) transformPaymentsAddress(address string) string {
    if w.Config.Payments.FlatLowercase {
        address = strings.ToLower(address)
    }
    return strings.TrimSpace(address)
}

// updateCurrentMonthPaymentsInfo updates sheet payments info into redis and update db users
// (set them active if they paid enough)
func (w *Worker) updateCurrentMonthPaymentsInfo() {
    now := time.Now()
    start := now.AddDate(0, 0, -now.Day() + 1)
    year, month, _ := start.Date()
	payments1 := w.paymentsSheet.GetMonthYearPayments(year, int(month))
    year, month, _ = start.AddDate(0, -1, 0).Date()
	payments2 := w.paymentsSheet.GetMonthYearPayments(year, int(month))
    year, month, _ = start.AddDate(0, -2, 0).Date()
	payments3 := w.paymentsSheet.GetMonthYearPayments(year, int(month))
	payments := w.sumPayments(payments1, payments2, payments3)

	// find changed info and update db
	for _, row := range payments {
		address := row[0]
		new_val := row[1]

		res, err := w.redisClient.HMGet(context.Background(), currentMonthPaymentsKey, address).Result()
		if err != nil {
			log.Println("Error getting hash field for update.", err)
		}
		redis_val := res[0]

		// if theres no such address in redis (first value in response is nil)
		if redis_val == nil {
			continue
		}

		// if value in redis and sheet differs check if value is enough
		if redis_val.(string) != new_val {
			w.checkUserPayment(address, new_val, w.activateUserAddress)
		}
	}

	// write results to redis
    flattened := turn2Dto1D(payments)
    keyvalues := make([]interface{}, len(flattened))
    for i, v := range flattened {
        keyvalues[i] = interface{}(v)
    }
	_, err := w.redisClient.HSet(context.Background(), currentMonthPaymentsKey, keyvalues...).Result()
	if err != nil {
        log.Println("[Payments] latest month payments:", err)
        log.Println(len(keyvalues))
		return
	}
}

// updateUnpaidMonthInfo updates info about not paid months in redis
func (w *Worker) updateUnpaidMonthInfo() {
	// due to sheet inconsistency start from predefined date
	var startYear = w.Config.Payments.YearStart
	var startMonth = w.Config.Payments.MonthStart

	currentYear, currentMonth, _ := time.Now().Date()

	var month int
	var monthMax = 12
	var payments [][]string

	for year := startYear; year <= currentYear; year++ {
		if year == startYear {
			month = startMonth
		} else {
			month = 1
		}
		// walk till current month
		if year == currentYear {
			monthMax = int(currentMonth)
		}

		for ; month <= monthMax; month++ {
			payments = w.paymentsSheet.GetMonthYearPayments(year, month)
			for _, row := range payments {
				address := row[0]
				value := row[1]
                // some citizens are msg guard employees. and they do not pay for the service.
                // so mark their payments as minimal
                if value == "*" {
                    value = strconv.Itoa(w.Config.Payments.PaymentThreshold)
                }
				checkedYearMonth := strconv.Itoa(year) + "." + fmt.Sprintf("%02d", month)

				var action func(context.Context, string, ...interface{}) *(redis.IntCmd)
				if w.hasUserPaidEnough(value) {
					action = w.redisClient.SRem
				} else {
					action = w.redisClient.SAdd
				}
				_, err := action(context.Background(), unpaidAddressKey+address, checkedYearMonth).Result()
				if err != nil {
					log.Println(err)
				}
			}
		}
	}
}

func (w *Worker) reactivatePaidUsers() {
	usersAndPayments, err := w.redisClient.HGetAll(context.Background(), currentMonthPaymentsKey).Result()
	if err != nil {
		log.Println("error during getting reactivation data:", err)
	}

	for address, payment := range usersAndPayments {
		w.checkUserPayment(address, payment, w.activateUserAddress)
	}
}

func (w *Worker) hasUserPaidEnough(value string) bool {
	price, err := strconv.ParseFloat(value, 64)
	if err != nil {
		return false
	}
	return price >= float64(w.Config.Payments.PaymentThreshold)
}

func (w *Worker) checkUserPayment(address string, value string, callback func(string)) {
	if w.hasUserPaidEnough(value) {
		callback(address)
	}
}

func (w *Worker) disableAllClientIfNeeded() {
	const disablingDay = 1
	// get from redis cached info
	disabledMonth, err := w.redisClient.Get(context.Background(), lastMonthOfDisablingKey).Int()
	if err != nil {
		log.Println(err)
	}
	// if its day of disabling and we haven't done this this day
	// disabledMonth == 0 means there's no such key in redis
	_, month, day := time.Now().Date()
	if disabledMonth == 0 || (day == disablingDay && int(month) != disabledMonth) {
		w.doDisableAllClients()
		w.reactivatePaidUsers()
	}
}

func (w *Worker) activateUserAddress(address string) {
	const query = "UPDATE users SET activated = TRUE WHERE address = $1 AND address_confirmed = TRUE;"
	_, err := w.postgresClient.Exec(query, address)
	if err != nil {
		log.Printf("Error while changing state for %v. %v", address, err)
		return
	}
}

func (w *Worker) doDisableAllClients() {
	log.Println("Disabling all clients in DB...")
	const query = "UPDATE users SET activated = FALSE WHERE address_confirmed = TRUE;"
	_, err := w.postgresClient.Exec(query)
	if err != nil {
		log.Println("Error disabling all client in DB.", err)
	} else {
		w.redisClient.Set(context.Background(), lastMonthOfDisablingKey, int(time.Now().Month()), time.Duration(0))
	}
}
