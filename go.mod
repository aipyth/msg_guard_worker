module worker

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.0 // indirect
	github.com/lib/pq v1.10.2 // indirect
	golang.org/x/tools/gopls v0.7.0 // indirect
	google.golang.org/api v0.51.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
