/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
package main

import (
	"worker/redisWorker"
)

func main() {
	worker := redisWorker.NewWorker()
	worker.Run()
}
