# the image is contains in gitlab container registry
image_name := registry.gitlab.com/aipyth/msg_guard_worker
sources := $(find . -name '.go')

.PHONY: all build build-image push-image

all: build-image push-image

build: $(sources)
	CGO_ENABLED=0 GOOS=linux go build -installsuffix cgo -o main .

build-image: $(sources)
	docker build -t $(image_name) .

push-image:
	docker push $(image_name)
